package m3uf5_practica1.vista;

import java.util.Scanner;


public class PersonaView {
    
 private Scanner scanner=new Scanner(System.in);
  public   String demanarText(String missatge){
        System.out.println(missatge);
        return scanner.nextLine();
  }
  
  public void imprimir(String text){
      System.out.println(text);
  }
  
  public String menu(){
      System.out.println("introduir una opció:\n+"
              + "1. Afegir"
              + "\n2. Cercar"
              + "\n3. Esborrar"
              + "\n4. Llistar"
              + "\n5. Sortir");
      return scanner.nextLine();
  }
}
