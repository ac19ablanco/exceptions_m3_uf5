package m3uf5_practica1.model;


public class DadesPersonaException  extends RuntimeException {

    public DadesPersonaException(String message) {
        super(message);
    }

    public DadesPersonaException(String message, Throwable cause) {
        super(message, cause);
    }
    

}
