
package m3uf5_practica1.model;

import java.util.List;

/**
 *
 * @author usuari
 */
public interface IGestioPersona {
  void afegir(Persona p) throws  GestioPersonaException;
  Persona cercar(String nif)throws GestioPersonaException ;
  void esborrar(String nif)throws  GestioPersonaException;
  List<Persona> getLlista();    
}
