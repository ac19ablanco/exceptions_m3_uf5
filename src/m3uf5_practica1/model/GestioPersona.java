package m3uf5_practica1.model;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class GestioPersona {

    private List<Persona> persones;

    public GestioPersona() {
        persones = new ArrayList();
    }

    public void afegir(Persona p) throws GestioPersonaException {
        comprovarPersona(p);
        if (!persones.contains(p)) {
            persones.add(p);
            LOGGER.log(Level.INFO, "Persona afegida: " + p.getNom());
        } else {
            
            LOGGER.log(Level.WARNING, "La persona que vols afegir ja existeix");
            throw new GestioPersonaException("Persona existent.");
        }
    }

    boolean comprovarPersona(Persona pr) {
        boolean esCorrecte = true;
        String miss = "";
        if (pr == null) {
            miss = "Persona null";
            esCorrecte = false;
        } else if (pr.getNif() == null || !comprovarNIF(pr.getNif())) {
            miss = "Error en el nif";
            esCorrecte = false;
        } else if (pr.getNom() == null || "".equals(pr.getNom())) {
            miss = "Error en el nom";
            esCorrecte = false;
        } else if (((Period.between(pr.getDataNaixement(), LocalDate.now()).getYears()) < 16
                || (Period.between(pr.getDataNaixement(), LocalDate.now()).getYears()) > 65)) {
            miss = "Edat introduida no esta entre 16 i 65 ";
            esCorrecte = false;
        }
        if (!esCorrecte) {
            throw new DadesPersonaException(miss);
        }

        return esCorrecte;
    }

    private boolean comprovarNIF(String nif) {
        return nif.matches("[0-9]{8}[A-Z]{1}");
    }

    public Persona cercar(String nif) throws GestioPersonaException {
        Persona exist;
        exist = null;
        for (Persona p : persones) {
            if (p.getNif().equalsIgnoreCase(nif)) {
                exist = p;
                LOGGER.log(Level.INFO, "Persona " + exist.getNom() + " existeix");
                break;

            } else {
                LOGGER.log(Level.WARNING, "Persona no existent");
                throw new GestioPersonaException("No s'ha trobat cap persona");

            }
        }

        return exist;
    }

    public void esborrar(String nif) throws GestioPersonaException {

        for (Persona p : persones) {
            if (p.getNif().equalsIgnoreCase(nif)) {
                LOGGER.log(Level.INFO, "Persona ", p.getNom() + " ha estat eliminada");
                persones.remove(p);
                break;
            } else {
                LOGGER.log(Level.WARNING, "No existeix aquest NIF");
                throw new GestioPersonaException("No existeix aquest NIF");

            }
        }
    }

    public List<Persona> getLlista() {
        return persones;
    }

}
