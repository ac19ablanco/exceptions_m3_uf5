package m3uf5_practica1.model;

import java.time.LocalDate;
import java.util.Objects;

public class Persona {

    private String nif;
    private String nom;
    private LocalDate dataNaixement;

    public Persona(String nif, String nom, LocalDate dataNaixement) {
        this.nif = nif;
        this.nom = nom;
        this.dataNaixement = dataNaixement;
    }

    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public LocalDate getDataNaixement() {
        return dataNaixement;
    }

    @Override
    public String toString() {
        return "nif=" + nif + ", nom=" + nom + ", dataNaixement=" + dataNaixement;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.nif);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.nif, other.nif)) {
            return false;
        }
        return true;
    }
    

}
