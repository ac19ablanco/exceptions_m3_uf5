package m3uf5_practica1.model;


public class GestioPersonaException extends Exception{

    public GestioPersonaException(String message) {
        super(message);
    }

    public GestioPersonaException(String message, Throwable cause) {
        super(message, cause);
    }
    

}
