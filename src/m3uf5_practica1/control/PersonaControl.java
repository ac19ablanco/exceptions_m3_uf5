package m3uf5_practica1.control;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import m3uf5_practica1.model.DadesPersonaException;
import m3uf5_practica1.model.GestioPersona;
import m3uf5_practica1.model.GestioPersonaException;
import m3uf5_practica1.model.Persona;
import m3uf5_practica1.vista.PersonaView;

public class PersonaControl {

    private GestioPersona gestio;
    private PersonaView view;

    public PersonaControl(GestioPersona gestio) {
        this.gestio = gestio;

    }

    public void iniciar() {
        while (true) {

            String accio = view.menu();

            switch (accio) {
                case "Afegir":
                    afegir();
                    break;
                case "Cercar":
                    cercar();
                    break;
                case "Esborrar":
                    esborrar();
                    break;
                case "Llistar":
                    llistar();
                    break;
                case "Sortir":
                    System.exit(0);
            }
        }
    }

    private void afegir() {
        
       try {
            //demanar al usuari introduir les dades de una persona.
            String nif = view.demanarText("Introdueix el nif").toUpperCase();
            String nom = view.demanarText("Introdueix el teu nom");
            LocalDate dataNascut = passarStringAData(view.demanarText("Introdueix la data de naixement 'DD-MM-YYYY'"));
            
            gestio.afegir(new Persona(nif, nom, dataNascut));
        } catch (GestioPersonaException | DadesPersonaException | DateTimeParseException ex) {

            System.out.println("Error al afegir aquesta persona: " + ex.getMessage() );
        }
    }


    private void cercar() {
         try {
            String nif = view.demanarText("Introduir nif");
            gestio.cercar(nif);
        } catch (GestioPersonaException | DadesPersonaException ex) {
            System.out.println("Error al cercar aquesta persona: " + ex.getMessage() );

        }
    }

    private void esborrar() {
        try {
            String nif = view.demanarText("Introduir nif");
            gestio.esborrar(nif);
        } catch (GestioPersonaException | DadesPersonaException ex) {
            System.out.println("Error al esborrar aquesta persona: " + ex.getMessage() );

        }
    }

    private void llistar() {
        System.out.println("Llista de persones" + gestio.getLlista());
    }
/**
 * Rep una data amb format dd-mm-yyyy i torna un LocalDate amb la data rebuda
 * @param dataText
 * @return
 * @throws DadesPersonaException, es llançarà si el string amb la data no compleix el format
 */
    private LocalDate passarStringAData(String dataText) {
        // es pot utilitzar el mètode parse de LocalDate

        return LocalDate.parse(dataText, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

}
